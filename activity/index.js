// 		-follow the naming conventions for functions.
// */
	
// 	//first function here:


function printWelcomeMessages() {
	let firstName = prompt("Enter Your First Name");
	let lastName = prompt("Enter Your Last Name");

	console.log("Hello, " + firstName + " " + lastName + "!");

	let age = prompt("How old are you?");
	console.log("You are " + age + " years old.");
	let address = prompt("Where do you live?");
	console.log("You live in " + address);
	
}
console.log("==============")
console.log("WELCOME USER: ")
console.log("==============")
printWelcomeMessages();


// /*
// 	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
// 		-invoke the function to display your information in the console.
// 		-follow the naming conventions for functions.
	
// */

// 	//second function here:


const topFiveFaveBands = ['Queen', 'Led Zeppelen', 'The Beatles', 'Adele', 'Ariana Grande']

function myFaveBands(value, index, array) {
	console.log((index+1) + ". "+ value);
}
console.log("================================")
console.log("My Favorite top 5 Music Artist: ")
console.log("================================")
topFiveFaveBands.forEach(myFaveBands);


// 	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
// 		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
// 		-invoke the function to display your information in the console.
// 		-follow the naming conventions for functions.



	
	// //third function here:

const topFiveMoviesRating = [
	{ title: "THE NOTEBOOK", rating: 85 },
	{ title: "THE VAMPIRE DIARIES", rating: 86 },
	{ title: "THE ORIGINALS", rating: 84 },
	{ title: "LUCIFER", rating: 85 },
	{ title: "THE HUNGER GAMES: COMPLETE 4-FILM COLLECTION", rating: 87 },
]

function myFaveMovies(value, index, array) {
	console.log((index+1) + ". "+ value.title);
	console.log("Rotten Tomatoes Rating: " + value.rating + "%");
}
console.log("==========================")
console.log("My Favorite top 5 Movies: ")
console.log("==========================")
topFiveMoviesRating.forEach(myFaveMovies);




	// 4. Debugging Practice - Debug the following codes and functions to avoid errors.
	// 	-check the variable names
	// 	-check the variable scope
	// 	-check function invocation/declaration
	// 	-comment out unusable codes.


// printUsers();
// let printFriends() = 

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

console.log("=====================")
console.log("You are friends with:")
console.log("=====================")
	
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printUsers();

// console.log(friend1);
// console.log(friend2);